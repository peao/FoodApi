
DROP DATABASE IF EXISTS foodApi;
CREATE DATABASE foodApi;
USE foodApi;

CREATE TABLE brands (
    id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE foods (
    id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    name VARCHAR(50) NOT NULL,
    brand BIGINT UNSIGNED NOT NULL,
    carbohydrate FLOAT UNSIGNED NOT NULL,
    protein FLOAT UNSIGNED NOT NULL,
    fat FLOAT UNSIGNED NOT NULL,
    FOREIGN KEY(brand) REFERENCES brands(id),
    PRIMARY KEY(id)
);

CREATE TABLE recipes (
    id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    name VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE recipe_steps (
    recipe BIGINT UNSIGNED NOT NULL,
    step_index INT UNSIGNED NOT NULL,
    instructions TEXT NOT NULL,
    FOREIGN KEY(recipe) REFERENCES recipes(id),
    PRIMARY KEY(recipe, step_index)
);

CREATE TABLE recipe_ingredients (
    recipe BIGINT UNSIGNED NOT NULL,
    food BIGINT UNSIGNED NOT NULL,
    amount INT UNSIGNED NOT NULL,
    unit ENUM('kg', 'hg', 'g', 'l', 'dl', 'cl', 'ml', 'teaspoon', 'tablespoon'),
    FOREIGN KEY(recipe) REFERENCES recipes(id),
    FOREIGN KEY(food) REFERENCES foods(id),
    PRIMARY KEY(recipe, food)
);

INSERT INTO brands (uuid, name)
VALUES (uuid(), 'Test brand');

INSERT INTO foods (uuid, name, brand, carbohydrate, protein, fat)
SELECT uuid(), 'Test food', brands.id, 33.3, 22.2, 11.1
FROM brands
WHERE brands.name = 'Test brand';

INSERT INTO recipes (uuid, name, description)
VALUES (uuid(), 'Test recipe', 'Test description for the test recipe');

INSERT INTO recipe_steps (recipe, step_index, instructions)
SELECT recipes.id, 0, 'First step for the test recipe'
FROM recipes
WHERE name = 'Test recipe';

INSERT INTO recipe_steps (recipe, step_index, instructions)
SELECT recipes.id, 1, 'Second step for the test recipe'
FROM recipes
WHERE name = 'Test recipe';

INSERT INTO recipe_steps (recipe, step_index, instructions)
SELECT recipes.id, 2, 'Third and final step for the test recipe'
FROM recipes
WHERE name = 'Test recipe';

INSERT INTO recipe_ingredients
SELECT recipes.id, foods.id, 5, 'dl'
FROM recipes, foods
WHERE recipes.name = 'Test recipe' AND foods.name = 'Test food';
