package com.peao;

import com.peao.Food.Food;
import com.peao.Food.MacrosException;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        String dbCredentials = System.getProperty("user.dir") + "/database/credentials";
        System.out.println(dbCredentials);
        Database db = new Database(dbCredentials);
        ArrayList<Food> foods = db.getAllFood();
        foods.forEach(f -> {
            System.out.println("Name: " + f.getName());
            System.out.println("Brand: " + f.getBrand());
            try {
                System.out.println("Carbs: " + String.valueOf(f.getCarbohydrate()));
                System.out.println("Protein: " + String.valueOf(f.getProtein()));
                System.out.println("Fat: " + String.valueOf(f.getFat()));
            } catch (MacrosException me) {
                System.out.println(me.getMessage());
            }
        });
    }
}
