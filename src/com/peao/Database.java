package com.peao;

import com.peao.Food.Food;
import com.peao.Food.MacrosException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Database {
    private final static String defaultHost = "localhost";
    private final static String defaultParams =
            "useSSL=false" +
            "&useJDBCCompliantTimezoneShift=true" +
            "&useLegacyDatetimeCode=false" +
            "&serverTimezone=UTC";
    private final static String urlBase = "jdbc:";

    private HashMap<String, String> connectionInfo;
    private Connection con;
    private Statement stmt;
    private ResultSet rs;

    private String url;
    private String user ;
    private String password;

    public Database(String filePath) {
        getConnectionInfoFromFile(filePath);
        setUpUrl();
    }

    public Database(String databaseType, String host, String port, String database, String user, String pw) {
        connectionInfo = new HashMap<>();
        connectionInfo.put("databaseType", databaseType);
        connectionInfo.put("host", host);
        connectionInfo.put("port", port);
        connectionInfo.put("database", database);
        connectionInfo.put("user", user);
        connectionInfo.put("pw", pw);
        setUpUrl();
    }

    public ArrayList<Food> getAllFood() {
        ArrayList<Food> foodList = new ArrayList<>();
        try {
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
            stmt.executeQuery("SELECT * FROM foods");

            rs = stmt.getResultSet();
            while(rs.next()) {
                foodList.add(new Food(
                        rs.getString("name"),
                        rs.getFloat("carbohydrate"),
                        rs.getFloat("protein"),
                        rs.getFloat("fat")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (MacrosException e) {
            e.printStackTrace();
        } finally {
            if(con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
                con = null;
            }
            if(stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
                stmt = null;
            }
            if(rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
                rs = null;
            }
        }
        return foodList;
    }

    private HashMap<String, String> getConnectionInfoFromFile(String filePath) {
        connectionInfo = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] tmp = line.split("\\s");
                connectionInfo.put(tmp[0], tmp[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(connectionInfo.toString());
        return connectionInfo;
    }

    private void setUpUrl() {
        setUpUrl(connectionInfo);
    }

    private void setUpUrl(HashMap<String, String> connectionInfo) {
        if(connectionInfo == null || connectionInfo.isEmpty()) {
            System.out.println("No connection info found");
            return;
        }

        setUpUrl(
                connectionInfo.get("databaseType"),
                connectionInfo.get("database"),
                connectionInfo.get("host"),
                connectionInfo.get("port"),
                connectionInfo.get("user"),
                connectionInfo.get("pw")
        );
    }

    private void setUpUrl(String databaseType, String database, String host, String port, String user, String password) {
        if(host == null) {
            host = defaultHost;
        }

        this.user = user;
        this.password = password;

        url = urlBase + databaseType + "://" + host;
        if(port != null) {
            url += ":" + port;
        }
        url += "/" + database + "?" + defaultParams;
    }
}
