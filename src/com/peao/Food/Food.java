package com.peao.Food;

/**
 * Represent a food it  em.
 */
public class Food extends Macros {
    private String name;
    private String brand;

    /**
     * Create default food item.
     * @throws MacrosException
     */
    public Food() throws MacrosException {
        this("UNKNOWN", 0.0f, 0.0f, 0.0f);
    }

    /**
     * Create food item with unknown brand.
     * @param name The name of the food item.
     * @param carbohydrate The amount of carbohydrate of the food item per 1 gram.
     * @param protein The amount of protein of the food item per 1 gram.
     * @param fat The amount of fat of the food item per 1 gram.
     * @throws MacrosException
     */
    public Food(String name, float carbohydrate, float protein, float fat) throws MacrosException {
        this(name, "UNKNOWN" , carbohydrate, protein, fat);
    }

    /**
     * Create food item and assumes macro values are per 1 gram of food.
     * @param name The name of the food item.
     * @param brand The brand of the food item.
     * @param carbohydrate The amount of carbohydrate of the food item per 1 gram.
     * @param protein The amount of protein of the food item per 1 gram.
     * @param fat The amount of fat of the food item per 1 gram.
     * @throws MacrosException
     */
    public Food(String name, String brand, float carbohydrate, float protein, float fat) throws MacrosException {
        this(name, brand, carbohydrate, protein, fat, 1);
    }

    /**
     * Create food item and normalize the macro values
     * @param name
     * @param brand
     * @param carbohydrate
     * @param protein
     * @param fat
     * @param grams
     * @throws MacrosException
     */
    public Food(String name, String brand, float carbohydrate, float protein, float fat, int grams) throws MacrosException {
        super(carbohydrate, protein, fat, grams);
        this.name = name;
        this.brand = brand;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
