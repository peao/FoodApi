package com.peao.Food;

public class MacrosException extends Exception {
    public MacrosException() {
        super();
    }

    public MacrosException(String message) {
        super(message);
    }
}
