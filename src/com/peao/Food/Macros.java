package com.peao.Food;

public class Macros {
    private final static int CALORIES_PER_CARBOHYDRATE = 4;
    private final static int CALORIES_PER_PROTEIN = 4;
    private final static int CALORIES_PER_FAT = 9;
    private final static String GRAMS_ZERO_OR_NEG_ERROR = "Grams cannot be zero or negative.";

    private float carbohydrate; // per g of food
    private float protein;      // per g of food
    private float fat;          // per g of food

    public Macros() throws MacrosException {
        this(0.0f, 0.0f, 0.0f, 1);
    }

    public Macros(float carbohydrate, float protein, float fat) throws MacrosException {
        this(carbohydrate, protein, fat, 1);
    }

    public Macros(float carbohydrate, float protein, float fat, int grams) throws MacrosException {
        if(grams < 1) {
            throw new MacrosException(GRAMS_ZERO_OR_NEG_ERROR);
        }

        this.carbohydrate = carbohydrate / grams;
        this.protein = protein / grams;
        this.fat = fat / grams;
    }

    public float getTotalCalories() {
        return this.getTotalCalories(1);
    }

    public float getTotalCalories(int grams) {
        return (carbohydrate * CALORIES_PER_CARBOHYDRATE +
                protein * CALORIES_PER_PROTEIN +
                fat * CALORIES_PER_FAT) * grams;
    }

    public float getCarbohydrate() throws MacrosException {
        return this.getCarbohydrate(1);
    }

    public float getCarbohydrate(int grams) throws MacrosException {
        if(grams < 1) {
            throw new MacrosException(GRAMS_ZERO_OR_NEG_ERROR);
        }

        return carbohydrate * grams;
    }

    public void setCarbohydrate(float carbohydrate) throws MacrosException {
        this.setCarbohydrate(carbohydrate, 1);
    }

    public void setCarbohydrate(float carbohydrate, int grams) throws MacrosException {
        if(grams < 1) {
            throw new MacrosException(GRAMS_ZERO_OR_NEG_ERROR);
        }

        this.carbohydrate = carbohydrate / grams;
    }

    public float getProtein() {
        return protein;
    }

    public void setProtein(float protein) throws  MacrosException {
        this.setProtein(protein, 1);
    }

    public void setProtein(float protein, int grams) throws MacrosException {
        if(grams < 1) {
            throw new MacrosException(GRAMS_ZERO_OR_NEG_ERROR);
        }

        this.protein = protein / grams;
    }

    public float getFat() {
        return fat;
    }

    public void setFat(float fat) throws MacrosException {
        this.setFat(fat, 1);
    }

    public void setFat(float fat, int grams) throws MacrosException{
        if(grams < 1) {
            throw new MacrosException(GRAMS_ZERO_OR_NEG_ERROR);
        }

        this.fat = fat / grams;
    }
}
